The API is opened under /api/ and provides the following endpoints:
* /photos/  -- GET (list all photos, no drafts, all users, sortable and filterable), POST (create a new photo, multipart form)
* /photos/{photoId}/ -- GET, PATCH, PUT, DELETE - usuall behaviour
* /photos/{photoId}/file/ -- GET - retreive the file associated witht the entry
* /user/photos/ -- GET list all photos of the current user, sortable
* /user/drafts/ -- GET list all the drafts of the current user, sortable


Test user on heroku: test/hmlet1234

Improvements to be made:
- all additional tasks
- restric actions on photos for not-owners
- store photos on a persistent filesystem


When learning Django I started off with a web-app, it can be accessed under /photos/ and is in very early stages
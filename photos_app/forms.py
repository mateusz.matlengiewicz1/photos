from django import forms
from .models import Photo

class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        fields = ['caption', 'picture', 'draft']
    # caption = forms.CharField(label='Caption of photo', max_length=1024)
    # draft = forms.BooleanField(label='Save as draft')

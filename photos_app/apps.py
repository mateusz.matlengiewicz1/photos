from django.apps import AppConfig


class PhotosAppConfig(AppConfig):
    name = 'photos_app'

from django.http import HttpResponseRedirect
from .models import Photo
from django.shortcuts import render, get_object_or_404
from django.views import View
from .forms import PhotoForm


# Create your views here.
class PhotosList(View):
    def get(self, request):
        latest_photos = Photo.objects.order_by('-pub_date')[:5]
        context = {'latest_photos': latest_photos}
        return render(request, 'photos/index.html', context)

    def post(self, request):
        form = PhotoForm(request.POST, request.FILES)
        form.save()
        return self.get(request)


class PhotoView(View):
    def get(self, request, photo_id):
        photo = get_object_or_404(Photo, pk=photo_id)
        return render(request, 'photos/detail.html', {'photo': photo})

    def post(self, request, photo_id):
        photo = get_object_or_404(Photo, pk=photo_id)
        form = PhotoForm(request.POST, request.FILES, instance=photo)
        # removing the required picture since we are editing
        form.base_fields['picture'].required = False
        form.full_clean()
        form.save()
        return self.get(request, photo_id)


def add_photo(request):
    form = PhotoForm()
    return render(request, 'photos/add.html', {'form': form})


def edit(request, photo_id):
    photo = get_object_or_404(Photo, pk=photo_id)
    form = PhotoForm(instance=photo)
    return render(request, 'photos/edit.html', {'form': form, 'id': photo_id})

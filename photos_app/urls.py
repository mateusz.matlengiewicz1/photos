from django.urls import path
from . import views

app_name = 'photos'
urlpatterns = [
    path('', views.PhotosList.as_view(), name='index'),
    path('add/', views.add_photo, name='add photo'),
    path('<int:photo_id>/', views.PhotoView.as_view(), name='details'),
    path('<int:photo_id>/edit', views.edit, name='edit'),
]

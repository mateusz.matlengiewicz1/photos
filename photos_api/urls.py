from django.urls import path
from . import views

app_name = 'photos_api'
urlpatterns = [
    path('photos/', views.PhotoList.as_view(), name='index'),
    path('photos/<int:pk>/', views.PhotoDetails.as_view(), name='details'),
    # to return the file itself we make a separate endpoint to serve media
    path('photos/<int:pk>/file/', views.PhotoFile.as_view(), name='file'),
    # for returning user only pictures and drafts we make a /user/ endpoint, since the userId is
    # the authenticated user from the JWT we don't need it in the path, that is also why the naming is singular
    path('user/photos/', views.UserPhotoList.as_view(), name='userIndex'),
    path('user/drafts/', views.UserDraftList.as_view(), name='userDrafts')
]


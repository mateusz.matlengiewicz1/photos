from django.test import TestCase
from . import views
import os, re
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory
from django.contrib.auth.models import User


class TestAPI(TestCase):
    fixtures = ['data.json', ]

    def test_photo_list(self):
        factory = APIRequestFactory()
        view = views.PhotoList.as_view()
        user = User.objects.get(username='test')
        request = factory.get('/photos/')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(6, len(response.data))

    def test_user_list(self):
        factory = APIRequestFactory()
        view = views.UserPhotoList.as_view()
        user = User.objects.get(username='test')
        request = factory.get('/user/photos/')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.data))

    def test_user_drat_list(self):
        factory = APIRequestFactory()
        view = views.UserDraftList.as_view()
        user = User.objects.get(username='test')
        request = factory.get('/user/drafts/')
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, len(response.data))

    def test_photo_details_notFound(self):
        factory = APIRequestFactory()
        view = views.PhotoDetails.as_view()
        user = User.objects.get(username='test')
        request = factory.get('/photos/1/')
        force_authenticate(request, user=user)
        response = view(request, pk=1)
        self.assertEqual(404, response.status_code)

    def test_photo_details_success(self):
        factory = APIRequestFactory()
        view = views.PhotoDetails.as_view()
        user = User.objects.get(username='test')
        request = factory.get('/photos/2/')
        force_authenticate(request, user=user)
        response = view(request, pk=2)
        self.assertEqual(200, response.status_code)
        self.assertEqual({'id': 2, 'caption': 'caption', 'draft': False, 'pub_date': '2020-04-17T11:23:04.058000Z'},
                         response.data)

    def test_create_photo(self):
        factory = APIRequestFactory()
        view = views.PhotoList.as_view()
        user = User.objects.get(username='test')
        full_filename = os.path.join("photos_api", "test_data", "ok.jpg")
        photo_data = {
            "file": (open(full_filename, "rb")),
            "caption": "test_caption"
        }
        request = factory.post('/photos/', photo_data)
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(201, response.status_code)

    def test_create_photo_file_too_big(self):
        factory = APIRequestFactory()
        view = views.PhotoList.as_view()
        user = User.objects.get(username='test')
        full_filename = os.path.join("photos_api", "test_data", "big.png")
        photo_data = {
            "file": (open(full_filename, "rb")),
            "caption": "test_caption"
        }
        request = factory.post('/photos/', photo_data)
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(400, response.status_code)
        self.assertEqual(["Picture size exceed limit 5mb"], response.data)

    def test_create_photo_file_dimensions_bad(self):
        factory = APIRequestFactory()
        view = views.PhotoList.as_view()
        user = User.objects.get(username='test')
        full_filename = os.path.join("photos_api", "test_data", "dimensions.jpg")
        photo_data = {
            "draft": True,
            "file": (open(full_filename, "rb")),
            "caption": "test_caption"
        }
        request = factory.post('/photos/', photo_data)
        force_authenticate(request, user=user)
        response = view(request)
        self.assertEqual(400, response.status_code)
        self.assertEqual(["Picture dimensions exceed limit 2000x1000"], response.data)

    def test_get_file_not_found(self):
        factory = APIRequestFactory()
        user = User.objects.get(username='test')
        request = factory.get('/photos/1/file/')
        force_authenticate(request, user=user)
        view = views.PhotoFile.as_view()
        response = view(request, 1)
        self.assertEqual(404, response.status_code)

    def test_get_file(self):
        factory = APIRequestFactory()
        view = views.PhotoList.as_view()
        user = User.objects.get(username='test')
        full_filename = os.path.join("photos_api", "test_data", "ok.jpg")
        photo_data = {
            "file": (open(full_filename, "rb")),
            "caption": "test_caption"
        }
        request = factory.post('/photos/', photo_data)
        force_authenticate(request, user=user)
        response = view(request)
        request = factory.get('/photos/%s/file/' % response.data['id'])
        force_authenticate(request, user=user)
        view = views.PhotoFile.as_view()
        response = view(request, response.data['id'])
        self.assertEqual(200, response.status_code)
        self.assertEqual(', "image"', response._content_type_for_repr)
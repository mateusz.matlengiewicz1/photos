from django.apps import AppConfig


class PhotosApiConfig(AppConfig):
    name = 'photos_api'

from rest_framework import serializers
from .models import Photo


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ("id", "caption", "draft", "pub_date")
        model = Photo


# custom serializer for POST request
class FullPhotoSerializer(serializers.ModelSerializer):
    file = serializers.ImageField(source='picture')

    class Meta:
        fields = ("id", "caption", "draft", "pub_date", "file")
        model = Photo

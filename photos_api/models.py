from django.db import models
from django.contrib.auth.models import User


class Photo(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    caption = models.CharField(max_length=1024, null=True)
    pub_date = models.DateTimeField(auto_now_add=True)
    draft = models.BooleanField(default=False)
    picture = models.ImageField(upload_to='pictures')

    def __str__(self):
        return self.id.__str__() + " " + self.caption

from django.utils import timezone
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.filters import OrderingFilter
from rest_framework.serializers import ValidationError

from .models import Photo
from .serializers import PhotoSerializer, FullPhotoSerializer


# Since there is no requirement to change the photo itself we can use the Serializer and a use a generic view
# to get the picture itself I will make another endpoint for the api

# after testing it works really well the only thing I am not sure about is why PUT and PATCH work the same way.
# I was thought that PUT would "reset" the object and null or the fields that are not provided in the body
# while PATCH only updates fields that are in the body
class PhotoDetails(RetrieveUpdateDestroyAPIView):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer


# additional view to get the picture as multipart
class PhotoFile(APIView):
    def get(self, request, pk):
        photo = get_object_or_404(Photo, pk=pk)
        # the content type should be better, there is a case of saving the type when creating the record
        return HttpResponse(photo.picture.read(), content_type='image')


class UserPhotoList(ListAPIView):
    filter_backends = [OrderingFilter]
    ordering_fields = ['pub_date']
    serializer_class = PhotoSerializer

    def get_queryset(self):
        return Photo.objects.filter(draft=False, owner=self.request.user)


class UserDraftList(ListAPIView):
    filter_backends = [OrderingFilter]
    ordering_fields = ['pub_date']
    serializer_class = PhotoSerializer

    def get_queryset(self):
        return Photo.objects.filter(draft=True, owner=self.request.user)


class PhotoList(ListAPIView):
    parser_classes = [MultiPartParser, ]
    filter_backends = [DjangoFilterBackend, OrderingFilter]
    # I assume drafts can only by accessed via the /user/drafts endpoint
    queryset = Photo.objects.filter(draft=False)
    filterset_fields = ['owner']
    ordering_fields = ['pub_date']

    # set the correct serializer based on the method
    def get_serializer_class(self):
        if self.request.method == "POST":
            return FullPhotoSerializer
        else:
            return PhotoSerializer

    def validate_picture(self, picture):
        if picture.size > 1024 * 1024 * 5:
            raise ValidationError("Picture size exceed limit 5mb")
        if picture.height > 1000 or picture.width > 2000:
            raise ValidationError("Picture dimensions exceed limit 2000x1000")

    def post(self, request):
        photo = Photo()
        photo.caption = request.POST.get('caption')
        photo.owner = request.user
        photo.pub_date = timezone.now()
        draft = request.POST.get('draft')
        if draft is not None:
            photo.draft = draft
        photo.picture = request.FILES['file']
        self.validate_picture(photo.picture)
        photo.save()
        return Response(PhotoSerializer(photo).data, status=201)

